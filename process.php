<?php
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die("Unauthorized access!");
}

// Validate and sanitize user inputs
$creditCardNumber = $_POST['cardNumber'];
$expirationMonth = $_POST['expiryMonth'];
$expirationYear = $_POST['expiryYear'];
$cvv = $_POST['cvv'];
$error_arr = [];
$validCreditCard = true;

// Validate credit card number using the Luhn algorithm
    if (!luhnValidate($creditCardNumber) || (strlen($creditCardNumber) < 16 || strlen($creditCardNumber) > 19) ) {

        $validCreditCard = false;
        $error_arr['message']['invalid_credit_card_number'] = "Invalid credit card number!";

    }

    // Check CVV length based on card type
    $isAmex = (substr($creditCardNumber, 0, 2) == '34' || substr($creditCardNumber, 0, 2) == '37');
    if ($isAmex && strlen($cvv) !== 4) {
        $validCreditCard = false;
        $error_arr['message']['invalid_security_code_for_amex'] = "Invalid cvv number on American Express credit card!";

    } elseif (!$isAmex && strlen($cvv) !== 3) {
        $cardName = '';
        if(substr($creditCardNumber, 0, 1) == '4') {
            $cardName = 'Visa';
        } else if(substr($creditCardNumber, 0, 2) == '51' || substr($creditCardNumber, 0, 2) == '52' || substr($creditCardNumber, 0, 2) == '53' || substr($creditCardNumber, 0, 2) == '54' || substr($creditCardNumber, 0, 2) == '55') {
            $cardName = 'MasterCard';
        }
        $validCreditCard = false;
        $error_arr['message']['invalid_security_code'] = "Invalid cvv number on the ${cardName} credit card!";
    }


    // Validate expiration month and year
    $currentMonth = date('m');
    $currentYear = date('Y');
    if ($expirationYear < $currentYear || ($expirationYear == $currentYear && $expirationMonth < $currentMonth)) {
        $validCreditCard = false;
        $error_arr['message']['invalid_expiration_date'] = "Invalid expiration date!";
    }

    // Combine expiration month and year into a single field
    $expirationDate = $expirationYear . '-' . $expirationMonth;
    $expiration_date_with_last_day = date('Y-m-t',strtotime($expirationDate));


    // Check if expiration_date is not null
    if ($expiration_date_with_last_day === '-') {
        $validCreditCard = false;
        $error_arr['message']['invalid_expiration_date'] = "Invalid expiration date!";

    }
    
    if(!$validCreditCard) {
        echo json_encode(['status' => 422, 'errors' => $error_arr]);
        return;
    }

    echo json_encode(['status' => 200, 'message' => "Successfully inserted card information!"]);

// Function to validate credit card number using Luhn algorithm
function luhnValidate($number) {
    $number = str_replace(' ', '', $number);

    if (!ctype_digit($number)) {
        // Input contains non-digit characters
        return false;
    }
    $sum = 0;
    $numDigits = strlen($number);
    $parity = $numDigits % 2;
    for ($i = 0; $i < $numDigits; $i++) {
        $digit = $number[$i];
        if ($i % 2 == $parity) {
            $digit *= 2;
            if ($digit > 9) {
                $digit -= 9;
            }
        }
        $sum += $digit;
    }
    return $sum % 10 == 0;
}
?>
