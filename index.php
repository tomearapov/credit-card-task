<?php

$old_credit_card = isset($_POST['credit_card']) ? $_POST['credit_card'] : '';
$oldExpiryMonth = isset($_POST['expiration_month']) ? $_POST['expiration_month'] : '';
$oldExpiryYear = isset($_POST['expiration_year']) ? $_POST['expiration_year'] : '';
$old_cvv = isset($_POST['cvv']) ? $_POST['cvv'] : '';

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Credit card information for the user</title>
    <!-- Compiled and minified Bootstrap 4.6.1 CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <!-- Font-Awesome CDN -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>

<body>

    <div class="container py-5">
        <div class="row">
            <div class="col-8 offset-2 my-2 text-center">
                <h1>Credit card information for the user</h1>
                <p class="bg-success text-white success"></p>
            </div>

        </div>
        <div class="row">
            <div class="col-8 offset-2">
                <form method="POST" action="process.php">

                    <div class="form-group">
                        <label for="credit_card">Credit Card Number:</label>
                        <div class="d-flex">
                            <input class="form-control" type="text" oninput="validateInput('credit_card'),getCardType(this.value),selectImage()" id="credit_card" maxlength="19" value="<?php echo $old_credit_card ?>">
                            <img id="card-logo" src="" class="ml-1 d-none">
                        </div>

                        <span id="cardNumberError" class="text-danger error"></span>
                    </div>

                    <div class="form-group">
                        <label for="expiration_month">Expiration Month:</label>
                        <select class="form-control" id="expiration_month">
                            <option value="">-- Select Month --</option>
                            <?php
                            $months = array(
                                '01' => 'January',
                                '02' => 'February',
                                '03' => 'March',
                                '04' => 'April',
                                '05' => 'May',
                                '06' => 'June',
                                '07' => 'July',
                                '08' => 'August',
                                '09' => 'September',
                                '10' => 'October',
                                '11' => 'November',
                                '12' => 'December',
                            );
                            foreach ($months as $value => $label) {
                                $selected = ($value == $oldExpiryMonth) ? 'selected' : '';
                                echo "<option value=\"$value\" $selected>$label</option>";
                            }
                            ?>
                        </select>

                    </div>

                    <div class="form-group">
                        <label for="expiration_year">Expiration Year:</label>
                        <select class="form-control" id="expiration_year">
                            <option value="">-- Select Year --</option>
                            <?php
                            $currentYear = date('Y');
                            $years = [$currentYear];

                            for ($i = 1; $i < 10; $i++) {
                                array_push($years, ($currentYear + $i));
                            }

                            foreach ($years as $year) {
                                $selected = ($year == $oldExpiryYear) ? 'selected' : '';
                                echo "<option value=\"$year\" $selected>$year</option>";
                            }
                            ?>
                        </select>
                        <span id="expiryCard" class="text-danger error"></span>

                    </div>

                    <div class="form-group">
                        <label for="cvv">CVV:</label>
                        <input class="form-control" type="text" name="cvv" maxlength="4" id="cvv" oninput="validateInput('cvv')" value="<?php echo $old_cvv ?>">
                        <span id="cvvError" class="error text-danger"></span>
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary btn-lg" value="Submit">
                    </div>

                </form>
            </div>



        </div>
    </div>

    <script>
        const form = document.querySelector('form');
        const selectedImage = document.getElementById('card-logo');

        function validateInput(id) {
            const inputField = document.getElementById(id);
            const inputValue = inputField.value;
            const numericValue = inputValue.replace(/\D/g, "");
            inputField.value = numericValue;
        }

        function getCardType(cardNumber) {
            if (/^4/.test(cardNumber)) {
                document.getElementById('cvv').setAttribute('maxlength', '3');
                return 'visa';
            } else if (/^5[1-5]/.test(cardNumber)) {
                document.getElementById('cvv').setAttribute('maxlength', '3');
                return 'mastercard';
            } else if (/^3[47]/.test(cardNumber)) {
                document.getElementById('cvv').setAttribute('maxlength', '4');
                return 'amex';
            } else {
                document.getElementById('cvv').setAttribute('maxlength', '3');
                return 'other';
            }
        }

        function selectImage() {

            const creditCardNumber = document.getElementById('credit_card').value;

            let imageSrc = '';



            if (creditCardNumber.startsWith('4')) {
                imageSrc = './images/visa.png';
                selectedImage.classList.remove('d-none');
            } else if (creditCardNumber.startsWith('51') || creditCardNumber.startsWith('52') || creditCardNumber.startsWith('53') || creditCardNumber.startsWith('54') || creditCardNumber.startsWith('55')) {
                imageSrc = './images/mastercard.png';
                selectedImage.classList.remove('d-none');
            } else if (creditCardNumber.startsWith('34') || creditCardNumber.startsWith('37')) {
                imageSrc = './images/american-express.png';
                selectedImage.classList.remove('d-none');

            } else {
                imageSrc = './images/default.png';
                selectedImage.classList.remove('d-none');
            }

            selectedImage.src = imageSrc;

        }

        form.addEventListener('submit', (event) => {
            event.preventDefault();

            // Clear previous error messages
            const errorElements = document.querySelectorAll('.error');
            const input_card_number = document.getElementById('credit_card');
            // input_card_number.addEventListener('input',selectedImage)

            document.querySelector('.success').textContent = '';
            // creditCardInput.addEventListener('input', selectImage);
            errorElements.forEach((errorElement) => {
                errorElement.textContent = '';
            });

            selectedImage.src = '';

            // Retrieve the form field values
            const cardNumber = document.getElementById('credit_card').value;
            const expiryYear = document.getElementById('expiration_year').value;
            const expiryMonth = document.getElementById('expiration_month').value;
            const cvv = document.getElementById('cvv').value;


            // Send the request to the PHP validation script
            async function fetchApi() {

                let validCard = true;

                // Check PAN (card number) length
                if (cardNumber.length < 16 || cardNumber.length > 19) {
                    document.getElementById('cardNumberError').textContent = 'Invalid card number. Please enter a valid card number.';
                    validCard = false;

                }

                // Check if expiry date is after the present time
                let currentDate = new Date();
                let currentYear = currentDate.getFullYear();
                let currentMonth = currentDate.getMonth() + 1; // January is 0, so we add 1
                if (expiryYear < currentYear || (expiryYear == currentYear && expiryMonth < currentMonth)) {
                    document.getElementById('expiryCard').textContent = 'Invalid expiry date. Please enter a valid date';
                    validCard = false;
                }


                // Check CVV length based on card type
                let cardType = getCardType(cardNumber);
                if ((cardType === 'amex' && cvv.length !== 4) || (cardType !== 'amex' && cvv.length !== 3)) {
                    document.getElementById('cvvError').textContent = 'Invalid CVV. Please enter a valid CVV.';
                    validCard = false;
                }

                if (!validCard) {
                    return false;
                }

                // Create the request body
                const requestBody = new FormData();
                requestBody.set('cardNumber', cardNumber)
                requestBody.set('expiryYear', expiryYear)
                requestBody.set('expiryMonth', expiryMonth)
                requestBody.set('cvv', cvv)



                const response = await fetch('process.php', {
                    method: 'POST',
                    body: requestBody
                });
                const data = await response.json()



                if (data.status == 200) {

                    // Validation on backend passed, perform further actions
                    document.querySelector('.success').textContent = data.message;
                    document.getElementById('credit_card').value = '';
                    document.getElementById('expiration_year').value = '';
                    document.getElementById('expiration_month').value = '';
                    document.getElementById('cvv').value = '';

                } else {

                    // Validation failed, display error messages


                    for (const key in data.errors.message) {

                        if (data.errors.message.hasOwnProperty(key)) {
                            if (key == 'invalid_credit_card_number') {
                                document.getElementById('cardNumberError').textContent = data.errors.message[key]
                            }
                            if (key == 'invalid_security_code_for_amex') {
                                document.getElementById('cvvError').textContent = data.errors.message[key]
                            }
                            if (key == 'invalid_security_code') {
                                document.getElementById('cvvError').textContent = data.errors.message[key]
                            }
                            if (key == 'invalid_expiration_date') {
                                document.getElementById('expiryCard').textContent = data.errors.message[key]
                            }
                        }
                    }


                }


            }

            fetchApi();
        });
    </script>
    <!-- jQuery library -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <!-- Compiled Bootstrap 4.6.1 JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
</body>

</html>